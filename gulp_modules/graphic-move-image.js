/* ==== IMPORT PARAMS ==== */

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const inDev = 'development';
const inDevApps = `${inDev}/components`;
const inPub = 'public';
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(

		src(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`),
		_run.size({ title: '[Pictures size] :' }),
		// _run.if(isPublic, _run.size()),
		dest(`${inPub}/media/img`)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
