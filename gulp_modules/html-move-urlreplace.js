/* ==== IMPORT PARAMS ==== */
import { lastRun } from 'gulp';
/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const
	inDev = 'development';
/* ==== ----- ==== */

/* ==== Replace URL or Links ==== */
const root = { a: '../..', b: '.' };
const __link = {
	srcPath: {
		in: `src="${root.a}/components/img/`,
		out: `src="${root.b}/media/img/`
	},
	urlPath: {
		in: `url\\(${root.a}/components/img/`,
		out: `url(${root.b}/media/img/`
	}
};
/* ==== ----- ==== */

const sinceReplace = x => `${x}`.replace(/-/gi, ':');

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(
		src(`${inDev}/*.html`, { since: lastRun(sinceReplace(nameTask)) }),
		_run.htmlReplace(__link.srcPath.in, __link.srcPath.out),
		_run.htmlReplace(__link.urlPath.in, __link.urlPath.out),
		dest(inDev)
	)
	&& combiner(
		src(`${inDev}/layouts/**/*.html`),
		_run.htmlReplace(__link.srcPath.in, __link.srcPath.out),
		_run.htmlReplace(__link.urlPath.in, __link.urlPath.out),
		dest(`${inDev}/layouts`)
	).on('error',
		_run.notify.onError(err => (errorConfig(`task: ${nameTask} `, 'ошибка!', err))));
