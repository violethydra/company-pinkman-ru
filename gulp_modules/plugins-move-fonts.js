/* ==== IMPORT PARAMS ==== */

'use strict';

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */
const
	inDev = 'development',
	inDevApps = `${inDev}/components`,
	inPub = 'public';
/* ==== ----- ==== */

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(

		src(`${inDevApps}/fonts/**/*.{ttf,eot,otf,woff,woff2}`, ''),
		_run.newer(`${inPub}/media/fonts`),
		dest(`${inPub}/media/fonts`)

	).on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));
