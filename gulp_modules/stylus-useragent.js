/* ==== IMPORT PARAMS ==== */
import resolver from 'stylus';
import changed from 'gulp-changed';
/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */

const	inDev = 'development';
const	inDevApps = `${inDev}/components`;
const	inPub = 'public';
const	inPubCss = `${inPub}/css`;
/* ==== ----- ==== */
 
 
/* ==== Replace URL or Links ==== */
const __cfg = {
	autoprefixer: {
		dev: {
			grid: false,
			cascade: false,
			flexbox: false,
			remove: true
		},
		pub: {
			grid: true,
			cascade: true,
			flexbox: true,
			remove: false
		}
	},
	stylus: {
		sprite: {
			import: `${__dirname}/../${inDev}/tmp/sprite`,
			define: { url: resolver() }
		}
	}
};
/* ==== ----- ==== */ 

/* ==== Replace URL or Links ==== */
const root = { a: '..' };
const __link = {
	css: {
		image: {
			prepend: `${root.a}/media/img/`
		},
		fonts: {
			replace: [`${root.a}/media/img/fonts/`, `${root.a}/media/fonts/`]
		}
	}
};
/* ==== ----- ==== */
 
module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(
		src(`${inDevApps}/stylus/**/firefox.styl`),
		_run.changed(inPubCss, { hasChanged: changed.compareContents}, { extension: '.css' }),
		_run.if(isDevelopment, _run.sourcemaps.init()),
		_run.stylus(__cfg.stylus.sprite),
		_run.if(isDevelopment, _run.autoprefixer(__cfg.autoprefixer.dev)),
		_run.if(isPublic, _run.autoprefixer(__cfg.autoprefixer.pub)),
		_run.if(isDevelopment, _run.sourcemaps.write()),
		_run.urlReplace(__link.css.image),
		_run.urlReplace(__link.css.fonts),
		_run.rename('firefox.css'),
		dest(inPubCss))
		
	&& combiner(src(`${inDevApps}/stylus/**/edge.styl`),
		_run.changed(inPubCss, { hasChanged: changed.compareContents}, { extension: '.css' }),
		_run.if(isDevelopment, _run.sourcemaps.init()),
		_run.stylus(__cfg.stylus.sprite),
		_run.if(isDevelopment, _run.autoprefixer(__cfg.autoprefixer.dev)),
		_run.if(isPublic, _run.autoprefixer(__cfg.autoprefixer.pub)),
		_run.if(isDevelopment, _run.sourcemaps.write()),
		_run.urlReplace(__link.css.image),
		_run.urlReplace(__link.css.fonts),
		_run.rename('edge.css'),
		dest(inPubCss))
		
	.on('error',
		_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'error!', err)));
