/* ==== IMPORT PARAMS ==== */

/* ==== ----- ==== */

/* ==== Sources and directions for files ==== */

	const inDev = 'development';
	const inDevApps = `${inDev}/components`;
	const inDevTmp = `${inDev}/tmp`;
/* ==== ----- ==== */

const __cfg = {
	svgmin: {
		js2svg: { pretty: false },
		plugins: [
			{ removeViewBox: false },
			{ addClassesToSVGElement: { className: 'mySvg' } },
			{ removeDoctype: true },
			{ removeComments: true }, 
			{ removeXMLProcInst: true },
			{ cleanupNumericValues: { floatPrecision: 2 } },
			{ convertColors: { names2hex: true, rgb2hex: true } },
			{ removeTitle: true },
			{ removeUselessStrokeAndFill: true },
			{ removeDesc: { removeAny: true } },
			{ convertTransform: true }
		]
	},
	cheerio: {
		run: ($, file) => {
				// elem.replace('#eb016e', 'currentColor');
			
			const recolor = ['fill'];
			recolor.forEach(f => ($(`[${f}]`).addClass('svg-recolor')));

			const ww = $('svg').attr('width');
			const hh = $('svg').attr('height');
			
			if (!$('svg').attr('viewBox')) {
				$('svg').attr('viewBox', `0 0 ${ww} ${hh}`);
			}
			
			['stroke="black"'].forEach(f => ($(`[${f}]`).removeAttr('stroke')));
			['stroke="#202E35"'].forEach(f => ($(`[${f}]`).removeAttr('stroke')));
			['fill'].forEach(f => ($(`[${f}]`).removeAttr('fill')));
			['opacity'].forEach(f => ($(`[${f}]`).removeAttr('opacity')));

			// const arr = ['fill', 'stroke', 'style'];
			const arr = ['style'];
			arr.forEach(f => ($(`[${f}]`).removeAttr(f)));
		},
		parserOptions: { xmlMode: true }
	},
	svgSymbols: {
		slug: { separator: '-' },
		id: 'id-glyphs-%f',
		class: '$%f$',
		templates: ['default-svg', 'default-stylus'],
		svgAttrs: { class: 'inline-svg__init', 'aria-hidden': 'true', version: '1.1' }
	}
};

module.exports = (nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig) =>
	() => combiner(
		src(`${inDevApps}/img/__glyphs/**/*.svg`, { allowEmpty: true }),
		_run.cheerio(__cfg.cheerio),
		_run.svgmin(__cfg.svgmin),
		_run.replace('&gt;', '>'),
		_run.replace('#EB016E', 'currentColor'),
		_run.svgSymbols(__cfg.svgSymbols),
		dest(`${inDevTmp}`)
	).on('error',
		_run.notify.onError(err => (errorConfig(`task: ${nameTask} `, 'ошибка!', err))));
