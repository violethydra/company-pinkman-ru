export default class $phoneInputMask {
	constructor(init) {
		this.input = init.inputid;
		this.mytarget = document.querySelector(`${this.input}`);
		this.regexp = /^[+|\d?][-\s.()/0-9]*$/;

		this.constructor.info();
		this.eventHandlers();
	}

	static info() { console.log('• [ADDON]:', this.name, true); }

	eventHandlers() {
		this.mytarget.addEventListener('input', event => this.main(event), false);
		this.mytarget.addEventListener('keydown', event => this.whatCode(event), false);
	}

	whatCode(event) {
		if (event.code === 'Backspace') this.mytarget.value = '';
	}

	main(event) {
		const space = ' ';
		const getPatter = event.target.getAttribute('data-pattern');
		const resultPattern = new RegExp(`^${getPatter}$`);
		const mask = this.mytarget.value.replace(/\D/g, '').match(resultPattern);

		if (this.mytarget.value.length === 0) return;
		if (!this.mytarget.value.match(this.regexp)) {
			this.mytarget.value = this.mytarget.value.slice(0, -1);
		} else {
			/**
			 * [value: if you see it, don't ask me: "What the hell is this, bro ?!]
			 * @type {[type]}
			 */
			this.mytarget.value = !mask[2] ? `+${mask[1]}${space}` : `+${mask[1]}${space}`
				+ (!mask[3] ? `(${mask[2]}` : `(${mask[2]})${space}`) + mask[3]
				+ (mask[4] ? `-${mask[4]}` : '')
				+ (mask[5] ? `-${mask[5]}` : '');
		}
	}
}
