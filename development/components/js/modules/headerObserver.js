export default class $headerObserver {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		if (!this.selector) return;
		this.outputHeight = document.querySelector(init.output);
		this.global = window;
	}

	static info() { console.log('[MODULE]:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.firstRun();
			this.eventHandlers();
		}
	}
	
	firstRun() {
		setTimeout(() => this.resizeHeight(), 100);
	}
	
	eventHandlers() {
		this.global.addEventListener('resize', event => this.resizeHeight(event), false);
	}
	
	resizeHeight() {
		this.windowSize = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		if (this.windowSize >= 992) this.outputHeight.style.height = '';
		else {
			this.getHeight = this.selector.offsetHeight;
			this.outputHeight.style.height = `${this.getHeight}px`;
		}
	}
}
