export default class $sliderSkin {
	constructor(init) {
		this.skin = init._whatSkin;
		this.result = init._result;
		this.prefix = init._prefix;
		this.text = init.text;
		this.svg = init.svg;
		this.url = init.url;
		this.post = init.post;
		this.constructor.info();
		this.main();
	}

	static info() { console.log('• [ADDON]:', this.name, true); }

	main() {
		this.themes = {
			default: `
				<a class="${this.prefix}__el" title="URL" role="link" href="${this.url}">
					<div class="${this.prefix}__el-item">
						<object class="${this.prefix}__el-svg">
						<svg role="picture-svg" class="glyphs__${this.svg}">
						<use xlink:href="#id-glyphs-${this.svg}"></use>
						</svg></object>
					</div>
					<div class="${this.prefix}__el-item">
						<p class="${this.prefix}__desc">${this.text}</p>
					</div>
				</a>
			`,
			press: `
				<a class="${this.prefix}__el" title="URL" role="link" href="${this.url}">
					<div class="${this.prefix}__el-item">
						<p class="${this.prefix}__data">${this.post}</p>
					</div>
					<div class="${this.prefix}__el-item">
						<p class="${this.prefix}__desc">${this.text}</p>
					</div>
					<div class="${this.prefix}__el-item">
						<object class="${this.prefix}__el-svg">
						<svg role="picture-svg" class="glyphs__${this.svg}">
						<use xlink:href="#id-glyphs-${this.svg}"></use>
						</svg></object>
					</div>
				</a>
			`,
			news: `
				<a class="${this.prefix}__el" title="URL" role="link" href="#">
					<div class="${this.prefix}__el-item">
						<p class="${this.prefix}__data">${this.post}</p>
					</div>
					<div class="${this.prefix}__el-item">
						<p class="${this.prefix}__desc">${this.text}</p>
					</div>
					<div class="${this.prefix}__el-item">
						<object class="card__svg-arrow">
						<svg role="picture-svg" class="glyphs__arrow-right">
						<use xlink:href="#id-glyphs-arrow-right"></use></svg>
						</object>
					</div>
				</a>
			`
		};

		if (this.skin) this.activeTheme = this.themes[this.skin];

		const div = document.createElement('DIV');
		div.classList = 'glide__slide';
		div.innerHTML = this.activeTheme || this.themes.default;

		this.result.appendChild(div);
	}
}
