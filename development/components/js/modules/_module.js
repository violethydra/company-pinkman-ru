export default class $module {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		if (!this.selector) return;
		this.log = 'example';
		// params
		
		// ---
	}

	static info() { console.log('[MODULE]:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.firstRun();
			this.eventHandlers();
		}
	}

	eventHandlers() {
		console.log(this.log);
	}

	firstRun() {
		this.main();
	}

	main() {
		this.lorem = `
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		`;

		console.log(this.lorem);

	}
}
