import Glide from '@glidejs/glide';
import $sliderSkin from './sliderThemes/sliderSkin';

export default class $sliderControle {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		if (!this.selector) return;

		this.selectorSlider = init.selectorSlider || '.js__sliderEnable';
		this.prefix = init.prefix;
		this.getList = init.getList || '.js__getList';
		this.pasteList = this.selector.querySelector(init.pasteList) || this.selector.querySelector('.js__pasteList');

		this.activeElem = this.selector.querySelector(init.activeElem) || this.selector.querySelector('.js__activeElem');
		this.totalElem = this.selector.querySelector(init.totalElem) || this.selector.querySelector('.js__totalElem');
		this.getBackground = init.background || '.js__getBackground';

		this.glideNumIndex = 0;
		this.glideNumView = 0;

		this.preView = init.preView || 1;
		this.sliderSkin = init.sliderSkin || 'default';
		
		this.skinUnique = init.unique || false;
	}

	static info() { console.log('[MODULE]:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.firstRun();
			this.eventHandlers();
		}
	}

	firstRun() {
		this.collectionList();
		this.createSliderSkin();
		this.mountSlider();
	}

	eventHandlers() {
		this.glide.on(['mount.after', 'run'], event => this.__refreshSlider(event), false);
		this.glide.on(['resize'], event => this.__refreshSlider(event), false);
	}

	__refreshSlider() {
		this.glideNumIndex = this.glide.index;
		this.glideNumView = this.glide.settings.perView;

		this.__calcCount();
	}

	collectionList() {
		this.arr = this.selector.querySelector(this.getList).children;
		this.arrCollection = [...this.arr].map((f, index) => ({
			text: (f.textContent || '').trim(),
			url: (f.getAttribute('data-url') || '').trim(),
			svg: (f.getAttribute('data-svgname') || '').trim(),
			post: (f.getAttribute('data-post') || '').trim().replace(/\//gi, '<span>/</span>')
		}));
	}

	__calcCount() {
		const x = this.pasteList.childElementCount || 0;
		this.totalElem.textContent = x;

		const y = Number(this.glideNumIndex + this.glideNumView) || 0;
		this.activeElem.textContent = y;
	}

	createSliderSkin() {
		this.arrCollection.forEach(f => new $sliderSkin({
			_whatSkin: this.sliderSkin,
			_result: this.pasteList,
			_prefix: this.prefix,
			text: f.text,
			svg: f.svg,
			url: f.url,
			post: f.post
		}));
		
		if (this.skinUnique) {
			console.warn('• [WARNING] Unique skin is:', true);
			
			const backPicture = this.selector.querySelector(this.getBackground);
			if (!backPicture) {
				console.error(`Unique skin is: true, but ${this.getBackground} not found!`);
				return;
			}
			this.getAttr = backPicture.getAttribute('data-background');

			const firstElem = this.pasteList.firstElementChild.querySelector('A');

			const uniqueDIV = document.createElement('DIV');
			uniqueDIV.classList = `${this.prefix}__el--background`;
			uniqueDIV.style.backgroundImage = `url(${this.getAttr || ''})`;

			firstElem.prepend(uniqueDIV);
			firstElem.classList.add(`${this.prefix}__unique`);
		}
	}

	mountSlider() {
		const keysNumb = [...Array(5).keys()].map(f => Math.round(2560 / (f + 1)));

		this.breakPoint = {};
		keysNumb.map((f, index) => {
			this.end = (this.preView - index);
			if (this.end <= 0) this.end = 1;
			return (this.breakPoint[f] = { perView: this.end });
		});

		this.glide = new Glide(this.selector.querySelector(this.selectorSlider), {
			type: 'slider',
			startAt: 0,
			perView: this.preView,
			bound: true,
			gap: 21,
			rewind: false,
			animationDuration: 300,
			breakpoints: this.breakPoint
		}).mount();

		this.glideNumIndex = this.glide.index;
		this.glideNumView = this.glide.settings.perView;
		this.__calcCount();
	}
}
