import $phoneInputMask from './iForm/phoneInputMask';

export default class $validatorInput {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		this.selectorID = init.selector;
		this.submitValid = false;

		this.button = document.querySelector(`${this.selectorID} button`);
		this.inputCollect = document.querySelectorAll(`${this.selectorID} input`);
	}

	static info() { console.log('[MODULE]:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.firstRun();
			this.eventHandlers();
		}
	}

	eventHandlers() {
		this.button.addEventListener('click', event => this.handlerClickButton(event), false);
		Array.from(this.inputCollect, f => f.addEventListener('input', event => this.handlerInputValid(event), false));
	}

	firstRun() {
		this.enablePhoneMask();
	}

	enablePhoneMask() {
		this.phone = new $phoneInputMask({
			selector: '[data-phone="true"]',
			inputid: `${this.selectorID}__phone`
		});
	}

	handlerClickButton(event) {
		this.submitValid = false;
		this.warning = document.querySelectorAll(`${this.selectorID} input.warning`);

		Array.from(this.inputCollect, f => this.__inputInvalid(f));

		if (this.warning.length === 0) this.submitValid = true;
		if (this.submitValid) this.__nextSubmit();
	}

	handlerInputValid(event) {
		this.t = event.target;
		if (this.t.value.length >= 2) this.t.classList.remove('warning');
	}

	__inputInvalid(item) {
		this.el = item;
		if (this.el.value.length <= 2) this.el.classList.add('warning');
	}

	__nextSubmit() {
		this.log = console.log('Отправлено!');
		[...this.inputCollect].forEach((el) => {
			const currentElem = el;
			currentElem.style.backgroundColor = '#0000';
			currentElem.style.color = '#fff3';
			currentElem.style.pointerEvents = 'none';
			currentElem.setAttribute('readonly', true);
		});

		this.button.style.pointerEvents = 'none';
		this.button.style.backgroundColor = '#5ccc2c';
		this.button.style.color = '#fff';
		this.button.innerText = 'Отправлено!';
		this.button.setAttribute('readonly', true);
	}
}
