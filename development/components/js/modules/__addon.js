export default class $addon {
	constructor(init) {
		this.param1 = init._param1;
		this.param2 = init._param2;
		this.prefix = init.prefix;

		this.constructor.info();
		this.main();
	}

	static info() { console.log('• [ADDON]:', this.name, true); }

	main() {
		console.log(this.param1, this.param2);
	}
}
