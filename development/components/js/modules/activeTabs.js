export default class $activeTabs {
	constructor(init) {
		this.selector = document.querySelector(init.selector);
		if (!this.selector) return;
		// --- NEXT ---
		this.errorText = '💀 Ouch, database error...';
		this.infoContent = document.querySelector(init.infoContent);
		this.dataParam = init.dataParam;
		
	}

	static info() { console.log('[MODULE]:', this.name, true); }

	run() {
		if (this.selector) {
			this.constructor.info();
			this.firstRun();
			this.eventHandlers();
		}
	}

	eventHandlers() {
		this.selector.addEventListener('click', event => this.listener(event), false);
	}

	firstRun() {
		this.selector.firstElementChild.setAttribute(this.dataParam, 'i-active');
		this.infoContent.firstElementChild.setAttribute(this.dataParam, 'i-active');
	}

	listener(event) {
		event.preventDefault();
		this.curElem = event.target;
		
		if (this.curElem === this.selector) return;
		if (this.curElem.hasAttribute(this.dataParam)) return;
		
		if (this.selector.querySelector(`[${this.dataParam}]`)) {
			this.selector.querySelector(`[${this.dataParam}]`).removeAttribute(this.dataParam);
		}
		if (this.infoContent.querySelector(`[${this.dataParam}]`)) {
			this.infoContent.querySelector(`[${this.dataParam}]`).removeAttribute(this.dataParam);
		}
		
		this.index = [...this.selector.children].indexOf(this.curElem);
		
		this.selector.children[this.index].setAttribute(this.dataParam, 'i-active');
		this.infoContent.children[this.index].setAttribute(this.dataParam, 'i-active');
	}
}
