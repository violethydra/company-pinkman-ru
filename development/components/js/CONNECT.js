// ============================
//    Name: index.js
// ============================

import $moveScrollUP from './modules/moveScrollUP';
import $openMyBurger from './modules/openMyBurger';
import $headerObserver from './modules/headerObserver';
import $sliderControle from './modules/sliderControle';
import $activeTabs from './modules/activeTabs';
import $validatorInput from './modules/validatorInput';

const startPackage = (option) => {
	console.log('[DOM]:', 'DOMContentLoaded', option);

	new $moveScrollUP({
		selector: '.js__moveScrollUP',
		speed: 8
	}).run();

	new $openMyBurger({
		burger: '.js__navHamburger',
		navbar: '.js__navHamburgerOpener',
		prefix: 'myheader'
	}).run();



	new $sliderControle({
		selector: '.js__solutionControl',
		selectorSlider: '.js__sliderEnable',
		pasteList: '.js__pasteList',
		getList: '.js__getList',
		activeElem: '.js__activeElem',
		totalElem: '.js__totalElem',
		prefix: 'solution',
		preView: 6
	}).run();

	new $activeTabs({
		selector: '.js__projectTabs',
		infoContent: '.js__projectTabsInfo',
		dataParam: 'data-state'
	}).run();

	new $sliderControle({
		selector: '.js__pressControl',
		prefix: 'press',
		sliderSkin: 'press',
		preView: 4
	}).run();
	
	new $sliderControle({
		selector: '.js__newsControl',
		prefix: 'news',
		sliderSkin: 'news',
		preView: 3,
		unique: true
	}).run();

	new $validatorInput({
		selector: '#answer'
	}).run();
	
	new $headerObserver({
		selector: '.js__headerObs',
		output: '.js__headerObsResize'
	}).run();
	
};

const start = (option) => {
	if (option === true) startPackage(option);
	else console.error('System: ', 'I think shit happens 😥 ');
};

const addCss = (fileName) => {
	const link = document.createElement('link');
	link.type = 'text/css';
	link.rel = 'stylesheet';
	link.href = fileName;

	document.head.appendChild(link);
};

if (typeof window !== 'undefined' && window && window.addEventListener) {

	if (navigator.userAgent.includes('Firefox')) addCss('css/firefox.css');
	if (navigator.userAgent.includes('Edge')) addCss('css/edge.css');

	document.addEventListener('DOMContentLoaded', start(true), false);
}
