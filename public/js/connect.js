/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"connect": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./development/components/js/connect.js","plugins"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules/moveScrollUP */ "./development/components/js/modules/moveScrollUP.js");
/* harmony import */ var _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/openMyBurger */ "./development/components/js/modules/openMyBurger.js");
/* harmony import */ var _modules_headerObserver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/headerObserver */ "./development/components/js/modules/headerObserver.js");
/* harmony import */ var _modules_sliderControle__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/sliderControle */ "./development/components/js/modules/sliderControle.js");
/* harmony import */ var _modules_activeTabs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/activeTabs */ "./development/components/js/modules/activeTabs.js");
/* harmony import */ var _modules_validatorInput__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/validatorInput */ "./development/components/js/modules/validatorInput.js");
// ============================
//    Name: index.js
// ============================







var startPackage = function startPackage(option) {
  console.log('[DOM]:', 'DOMContentLoaded', option);
  new _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_0__["default"]({
    selector: '.js__moveScrollUP',
    speed: 8
  }).run();
  new _modules_openMyBurger__WEBPACK_IMPORTED_MODULE_1__["default"]({
    burger: '.js__navHamburger',
    navbar: '.js__navHamburgerOpener',
    prefix: 'myheader'
  }).run();
  new _modules_sliderControle__WEBPACK_IMPORTED_MODULE_3__["default"]({
    selector: '.js__solutionControl',
    selectorSlider: '.js__sliderEnable',
    pasteList: '.js__pasteList',
    getList: '.js__getList',
    activeElem: '.js__activeElem',
    totalElem: '.js__totalElem',
    prefix: 'solution',
    preView: 6
  }).run();
  new _modules_activeTabs__WEBPACK_IMPORTED_MODULE_4__["default"]({
    selector: '.js__projectTabs',
    infoContent: '.js__projectTabsInfo',
    dataParam: 'data-state'
  }).run();
  new _modules_sliderControle__WEBPACK_IMPORTED_MODULE_3__["default"]({
    selector: '.js__pressControl',
    prefix: 'press',
    sliderSkin: 'press',
    preView: 4
  }).run();
  new _modules_sliderControle__WEBPACK_IMPORTED_MODULE_3__["default"]({
    selector: '.js__newsControl',
    prefix: 'news',
    sliderSkin: 'news',
    preView: 3,
    unique: true
  }).run();
  new _modules_validatorInput__WEBPACK_IMPORTED_MODULE_5__["default"]({
    selector: '#answer'
  }).run();
  new _modules_headerObserver__WEBPACK_IMPORTED_MODULE_2__["default"]({
    selector: '.js__headerObs',
    output: '.js__headerObsResize'
  }).run();
};

var start = function start(option) {
  if (option === true) startPackage(option);else console.error('System: ', 'I think shit happens 😥 ');
};

var addCss = function addCss(fileName) {
  var link = document.createElement('link');
  link.type = 'text/css';
  link.rel = 'stylesheet';
  link.href = fileName;
  document.head.appendChild(link);
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  if (navigator.userAgent.includes('Firefox')) addCss('css/firefox.css');
  if (navigator.userAgent.includes('Edge')) addCss('css/edge.css');
  document.addEventListener('DOMContentLoaded', start(true), false);
}

/***/ }),

/***/ "./development/components/js/modules/activeTabs.js":
/*!*********************************************************!*\
  !*** ./development/components/js/modules/activeTabs.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $activeTabs; });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $activeTabs =
/*#__PURE__*/
function () {
  function $activeTabs(init) {
    _classCallCheck(this, $activeTabs);

    this.selector = document.querySelector(init.selector);
    if (!this.selector) return; // --- NEXT ---

    this.errorText = '💀 Ouch, database error...';
    this.infoContent = document.querySelector(init.infoContent);
    this.dataParam = init.dataParam;
  }

  _createClass($activeTabs, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.firstRun();
        this.eventHandlers();
      }
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.selector.addEventListener('click', function (event) {
        return _this.listener(event);
      }, false);
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      this.selector.firstElementChild.setAttribute(this.dataParam, 'i-active');
      this.infoContent.firstElementChild.setAttribute(this.dataParam, 'i-active');
    }
  }, {
    key: "listener",
    value: function listener(event) {
      event.preventDefault();
      this.curElem = event.target;
      if (this.curElem === this.selector) return;
      if (this.curElem.hasAttribute(this.dataParam)) return;

      if (this.selector.querySelector("[".concat(this.dataParam, "]"))) {
        this.selector.querySelector("[".concat(this.dataParam, "]")).removeAttribute(this.dataParam);
      }

      if (this.infoContent.querySelector("[".concat(this.dataParam, "]"))) {
        this.infoContent.querySelector("[".concat(this.dataParam, "]")).removeAttribute(this.dataParam);
      }

      this.index = _toConsumableArray(this.selector.children).indexOf(this.curElem);
      this.selector.children[this.index].setAttribute(this.dataParam, 'i-active');
      this.infoContent.children[this.index].setAttribute(this.dataParam, 'i-active');
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('[MODULE]:', this.name, true);
    }
  }]);

  return $activeTabs;
}();



/***/ }),

/***/ "./development/components/js/modules/headerObserver.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/headerObserver.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $headerObserver; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $headerObserver =
/*#__PURE__*/
function () {
  function $headerObserver(init) {
    _classCallCheck(this, $headerObserver);

    this.selector = document.querySelector(init.selector);
    if (!this.selector) return;
    this.outputHeight = document.querySelector(init.output);
    this.global = window;
  }

  _createClass($headerObserver, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.firstRun();
        this.eventHandlers();
      }
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      var _this = this;

      setTimeout(function () {
        return _this.resizeHeight();
      }, 100);
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this2 = this;

      this.global.addEventListener('resize', function (event) {
        return _this2.resizeHeight(event);
      }, false);
    }
  }, {
    key: "resizeHeight",
    value: function resizeHeight() {
      this.windowSize = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      if (this.windowSize >= 992) this.outputHeight.style.height = '';else {
        this.getHeight = this.selector.offsetHeight;
        this.outputHeight.style.height = "".concat(this.getHeight, "px");
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('[MODULE]:', this.name, true);
    }
  }]);

  return $headerObserver;
}();



/***/ }),

/***/ "./development/components/js/modules/iForm/phoneInputMask.js":
/*!*******************************************************************!*\
  !*** ./development/components/js/modules/iForm/phoneInputMask.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $phoneInputMask; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $phoneInputMask =
/*#__PURE__*/
function () {
  function $phoneInputMask(init) {
    _classCallCheck(this, $phoneInputMask);

    this.input = init.inputid;
    this.mytarget = document.querySelector("".concat(this.input));
    this.regexp = /^[+|\d?][-\s.()/0-9]*$/;
    this.constructor.info();
    this.eventHandlers();
  }

  _createClass($phoneInputMask, [{
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.mytarget.addEventListener('input', function (event) {
        return _this.main(event);
      }, false);
      this.mytarget.addEventListener('keydown', function (event) {
        return _this.whatCode(event);
      }, false);
    }
  }, {
    key: "whatCode",
    value: function whatCode(event) {
      if (event.code === 'Backspace') this.mytarget.value = '';
    }
  }, {
    key: "main",
    value: function main(event) {
      var space = ' ';
      var getPatter = event.target.getAttribute('data-pattern');
      var resultPattern = new RegExp("^".concat(getPatter, "$"));
      var mask = this.mytarget.value.replace(/\D/g, '').match(resultPattern);
      if (this.mytarget.value.length === 0) return;

      if (!this.mytarget.value.match(this.regexp)) {
        this.mytarget.value = this.mytarget.value.slice(0, -1);
      } else {
        /**
         * [value: if you see it, don't ask me: "What the hell is this, bro ?!]
         * @type {[type]}
         */
        this.mytarget.value = !mask[2] ? "+".concat(mask[1]).concat(space) : "+".concat(mask[1]).concat(space) + (!mask[3] ? "(".concat(mask[2]) : "(".concat(mask[2], ")").concat(space)) + mask[3] + (mask[4] ? "-".concat(mask[4]) : '') + (mask[5] ? "-".concat(mask[5]) : '');
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('• [ADDON]:', this.name, true);
    }
  }]);

  return $phoneInputMask;
}();



/***/ }),

/***/ "./development/components/js/modules/moveScrollUP.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/moveScrollUP.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $moveScrollUP; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $moveScrollUP =
/*#__PURE__*/
function () {
  function $moveScrollUP(init) {
    _classCallCheck(this, $moveScrollUP);

    this.selector = document.querySelector(init.selector);
    if (!this.selector) return;
    this.count = Number(Math.abs(init.speed)) || 8;
    this.speed = this.count <= 20 ? this.count : 8;
    this.myPos = window.pageYOffset;
    this.getScroll = 0;
    this.speedScroll = this.speed;
  }

  _createClass($moveScrollUP, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.eventHandlers();
      }
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.selector.addEventListener('click', function (event) {
        return _this.clickedArrow(event);
      }, false);
      window.addEventListener('scroll', function (event) {
        return _this.scrolledDown(event);
      }, false);
    }
  }, {
    key: "clickedArrow",
    value: function clickedArrow(event) {
      this.getScroll = document.documentElement.scrollTop;

      if (this.getScroll >= 1) {
        window.requestAnimationFrame(this.clickedArrow.bind(this));
        window.scrollTo(0, this.getScroll - this.getScroll / this.speedScroll);
      }
    }
  }, {
    key: "scrolledDown",
    value: function scrolledDown(event) {
      this.myPos = window.pageYOffset;
      if (this.myPos >= 100) this.selector.classList.add('show');else this.selector.classList.remove('show');
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('[MODULE]:', this.name, true);
    }
  }]);

  return $moveScrollUP;
}();



/***/ }),

/***/ "./development/components/js/modules/openMyBurger.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/openMyBurger.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $openMyBurger; });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $openMyBurger =
/*#__PURE__*/
function () {
  function $openMyBurger(init) {
    _classCallCheck(this, $openMyBurger);

    this.selector = document.querySelector(init.burger);
    if (!this.selector) return;
    this.prefix = init.prefix;
    this.navbar = document.querySelector(init.navbar);
    this.errorText = '💀 Ouch, database error...';
  }

  _createClass($openMyBurger, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.firstRun();
        this.eventHandlers();
      }
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.selector.addEventListener('click', function (event) {
        return _this.handlearOpenBurger(event);
      }, false);
      window.addEventListener('keydown', function (event) {
        return _this.handlearOpenBurgerESC(event);
      }, false);
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      this.createMobileLinks();
    }
  }, {
    key: "handlearOpenBurgerESC",
    value: function handlearOpenBurgerESC(event) {
      if (this.navbar.classList.contains('open')) {
        if (event.key === 'Escape') this.navbar.classList.remove('open');
      }
    }
  }, {
    key: "handlearOpenBurger",
    value: function handlearOpenBurger(event) {
      if (this.navbar.classList.contains('open')) this.navbar.classList.remove('open');else this.navbar.classList.add('open');
    }
  }, {
    key: "createMobileLinks",
    value: function createMobileLinks() {
      var _this2 = this;

      var link = document.querySelectorAll('.js__headerText');
      var linkFirst = document.querySelectorAll('.js__headerText')[0].childElementCount || 0;

      var resultMap = function resultMap(index) {
        _this2.ar = _toConsumableArray(link[index].children).map(function (g) {
          return {
            name: g.textContent || '',
            url: g.attributes.href.textContent || ''
          };
        });
        return _this2.covnertArr.push(_this2.ar);
      };

      this.covnertArr = [];
      this.newArr = _toConsumableArray(link).map(function (f, index) {
        return resultMap(index);
      });
      this.result = [];
      this.covnertArr.reverse().map(function (f) {
        var _this2$result;

        return (_this2$result = _this2.result).push.apply(_this2$result, _toConsumableArray(f));
      });
      var div = document.createElement('DIV');
      div.classList = "".concat(this.prefix, "__group ").concat(this.prefix, "__clone-list");
      this.creatHref = this.navbar.querySelector('DIV').appendChild(div);
      this.result.map(function (f, index) {
        var myHref = document.createElement('a');
        if (index < linkFirst) myHref.classList = "".concat(_this2.prefix, "__clone-item ").concat(_this2.prefix, "__clone--style");else myHref.classList = "".concat(_this2.prefix, "__clone-item");
        myHref.textContent = f.name || '';
        myHref.href = f.url || '';
        return _this2.creatHref.appendChild(myHref);
      });
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('[MODULE]:', this.name, true);
    }
  }]);

  return $openMyBurger;
}();



/***/ }),

/***/ "./development/components/js/modules/sliderControle.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/sliderControle.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $sliderControle; });
/* harmony import */ var _glidejs_glide__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @glidejs/glide */ "./node_modules/@glidejs/glide/dist/glide.esm.js");
/* harmony import */ var _sliderThemes_sliderSkin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sliderThemes/sliderSkin */ "./development/components/js/modules/sliderThemes/sliderSkin.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var $sliderControle =
/*#__PURE__*/
function () {
  function $sliderControle(init) {
    _classCallCheck(this, $sliderControle);

    this.selector = document.querySelector(init.selector);
    if (!this.selector) return;
    this.selectorSlider = init.selectorSlider || '.js__sliderEnable';
    this.prefix = init.prefix;
    this.getList = init.getList || '.js__getList';
    this.pasteList = this.selector.querySelector(init.pasteList) || this.selector.querySelector('.js__pasteList');
    this.activeElem = this.selector.querySelector(init.activeElem) || this.selector.querySelector('.js__activeElem');
    this.totalElem = this.selector.querySelector(init.totalElem) || this.selector.querySelector('.js__totalElem');
    this.getBackground = init.background || '.js__getBackground';
    this.glideNumIndex = 0;
    this.glideNumView = 0;
    this.preView = init.preView || 1;
    this.sliderSkin = init.sliderSkin || 'default';
    this.skinUnique = init.unique || false;
  }

  _createClass($sliderControle, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.firstRun();
        this.eventHandlers();
      }
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      this.collectionList();
      this.createSliderSkin();
      this.mountSlider();
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.glide.on(['mount.after', 'run'], function (event) {
        return _this.__refreshSlider(event);
      }, false);
      this.glide.on(['resize'], function (event) {
        return _this.__refreshSlider(event);
      }, false);
    }
  }, {
    key: "__refreshSlider",
    value: function __refreshSlider() {
      this.glideNumIndex = this.glide.index;
      this.glideNumView = this.glide.settings.perView;

      this.__calcCount();
    }
  }, {
    key: "collectionList",
    value: function collectionList() {
      this.arr = this.selector.querySelector(this.getList).children;
      this.arrCollection = _toConsumableArray(this.arr).map(function (f, index) {
        return {
          text: (f.textContent || '').trim(),
          url: (f.getAttribute('data-url') || '').trim(),
          svg: (f.getAttribute('data-svgname') || '').trim(),
          post: (f.getAttribute('data-post') || '').trim().replace(/\//gi, '<span>/</span>')
        };
      });
    }
  }, {
    key: "__calcCount",
    value: function __calcCount() {
      var x = this.pasteList.childElementCount || 0;
      this.totalElem.textContent = x;
      var y = Number(this.glideNumIndex + this.glideNumView) || 0;
      this.activeElem.textContent = y;
    }
  }, {
    key: "createSliderSkin",
    value: function createSliderSkin() {
      var _this2 = this;

      this.arrCollection.forEach(function (f) {
        return new _sliderThemes_sliderSkin__WEBPACK_IMPORTED_MODULE_1__["default"]({
          _whatSkin: _this2.sliderSkin,
          _result: _this2.pasteList,
          _prefix: _this2.prefix,
          text: f.text,
          svg: f.svg,
          url: f.url,
          post: f.post
        });
      });

      if (this.skinUnique) {
        console.warn('• [WARNING] Unique skin is:', true);
        var backPicture = this.selector.querySelector(this.getBackground);

        if (!backPicture) {
          console.error("Unique skin is: true, but ".concat(this.getBackground, " not found!"));
          return;
        }

        this.getAttr = backPicture.getAttribute('data-background');
        var firstElem = this.pasteList.firstElementChild.querySelector('A');
        var uniqueDIV = document.createElement('DIV');
        uniqueDIV.classList = "".concat(this.prefix, "__el--background");
        uniqueDIV.style.backgroundImage = "url(".concat(this.getAttr || '', ")");
        firstElem.prepend(uniqueDIV);
        firstElem.classList.add("".concat(this.prefix, "__unique"));
      }
    }
  }, {
    key: "mountSlider",
    value: function mountSlider() {
      var _this3 = this;

      var keysNumb = _toConsumableArray(Array(5).keys()).map(function (f) {
        return Math.round(2560 / (f + 1));
      });

      this.breakPoint = {};
      keysNumb.map(function (f, index) {
        _this3.end = _this3.preView - index;
        if (_this3.end <= 0) _this3.end = 1;
        return _this3.breakPoint[f] = {
          perView: _this3.end
        };
      });
      this.glide = new _glidejs_glide__WEBPACK_IMPORTED_MODULE_0__["default"](this.selector.querySelector(this.selectorSlider), {
        type: 'slider',
        startAt: 0,
        perView: this.preView,
        bound: true,
        gap: 21,
        rewind: false,
        animationDuration: 300,
        breakpoints: this.breakPoint
      }).mount();
      this.glideNumIndex = this.glide.index;
      this.glideNumView = this.glide.settings.perView;

      this.__calcCount();
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('[MODULE]:', this.name, true);
    }
  }]);

  return $sliderControle;
}();



/***/ }),

/***/ "./development/components/js/modules/sliderThemes/sliderSkin.js":
/*!**********************************************************************!*\
  !*** ./development/components/js/modules/sliderThemes/sliderSkin.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $sliderSkin; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var $sliderSkin =
/*#__PURE__*/
function () {
  function $sliderSkin(init) {
    _classCallCheck(this, $sliderSkin);

    this.skin = init._whatSkin;
    this.result = init._result;
    this.prefix = init._prefix;
    this.text = init.text;
    this.svg = init.svg;
    this.url = init.url;
    this.post = init.post;
    this.constructor.info();
    this.main();
  }

  _createClass($sliderSkin, [{
    key: "main",
    value: function main() {
      this.themes = {
        "default": "\n\t\t\t\t<a class=\"".concat(this.prefix, "__el\" title=\"URL\" role=\"link\" href=\"").concat(this.url, "\">\n\t\t\t\t\t<div class=\"").concat(this.prefix, "__el-item\">\n\t\t\t\t\t\t<object class=\"").concat(this.prefix, "__el-svg\">\n\t\t\t\t\t\t<svg role=\"picture-svg\" class=\"glyphs__").concat(this.svg, "\">\n\t\t\t\t\t\t<use xlink:href=\"#id-glyphs-").concat(this.svg, "\"></use>\n\t\t\t\t\t\t</svg></object>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"").concat(this.prefix, "__el-item\">\n\t\t\t\t\t\t<p class=\"").concat(this.prefix, "__desc\">").concat(this.text, "</p>\n\t\t\t\t\t</div>\n\t\t\t\t</a>\n\t\t\t"),
        press: "\n\t\t\t\t<a class=\"".concat(this.prefix, "__el\" title=\"URL\" role=\"link\" href=\"").concat(this.url, "\">\n\t\t\t\t\t<div class=\"").concat(this.prefix, "__el-item\">\n\t\t\t\t\t\t<p class=\"").concat(this.prefix, "__data\">").concat(this.post, "</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"").concat(this.prefix, "__el-item\">\n\t\t\t\t\t\t<p class=\"").concat(this.prefix, "__desc\">").concat(this.text, "</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"").concat(this.prefix, "__el-item\">\n\t\t\t\t\t\t<object class=\"").concat(this.prefix, "__el-svg\">\n\t\t\t\t\t\t<svg role=\"picture-svg\" class=\"glyphs__").concat(this.svg, "\">\n\t\t\t\t\t\t<use xlink:href=\"#id-glyphs-").concat(this.svg, "\"></use>\n\t\t\t\t\t\t</svg></object>\n\t\t\t\t\t</div>\n\t\t\t\t</a>\n\t\t\t"),
        news: "\n\t\t\t\t<a class=\"".concat(this.prefix, "__el\" title=\"URL\" role=\"link\" href=\"#\">\n\t\t\t\t\t<div class=\"").concat(this.prefix, "__el-item\">\n\t\t\t\t\t\t<p class=\"").concat(this.prefix, "__data\">").concat(this.post, "</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"").concat(this.prefix, "__el-item\">\n\t\t\t\t\t\t<p class=\"").concat(this.prefix, "__desc\">").concat(this.text, "</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"").concat(this.prefix, "__el-item\">\n\t\t\t\t\t\t<object class=\"card__svg-arrow\">\n\t\t\t\t\t\t<svg role=\"picture-svg\" class=\"glyphs__arrow-right\">\n\t\t\t\t\t\t<use xlink:href=\"#id-glyphs-arrow-right\"></use></svg>\n\t\t\t\t\t\t</object>\n\t\t\t\t\t</div>\n\t\t\t\t</a>\n\t\t\t")
      };
      if (this.skin) this.activeTheme = this.themes[this.skin];
      var div = document.createElement('DIV');
      div.classList = 'glide__slide';
      div.innerHTML = this.activeTheme || this.themes["default"];
      this.result.appendChild(div);
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('• [ADDON]:', this.name, true);
    }
  }]);

  return $sliderSkin;
}();



/***/ }),

/***/ "./development/components/js/modules/validatorInput.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/validatorInput.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return $validatorInput; });
/* harmony import */ var _iForm_phoneInputMask__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./iForm/phoneInputMask */ "./development/components/js/modules/iForm/phoneInputMask.js");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var $validatorInput =
/*#__PURE__*/
function () {
  function $validatorInput(init) {
    _classCallCheck(this, $validatorInput);

    this.selector = document.querySelector(init.selector);
    this.selectorID = init.selector;
    this.submitValid = false;
    this.button = document.querySelector("".concat(this.selectorID, " button"));
    this.inputCollect = document.querySelectorAll("".concat(this.selectorID, " input"));
  }

  _createClass($validatorInput, [{
    key: "run",
    value: function run() {
      if (this.selector) {
        this.constructor.info();
        this.firstRun();
        this.eventHandlers();
      }
    }
  }, {
    key: "eventHandlers",
    value: function eventHandlers() {
      var _this = this;

      this.button.addEventListener('click', function (event) {
        return _this.handlerClickButton(event);
      }, false);
      Array.from(this.inputCollect, function (f) {
        return f.addEventListener('input', function (event) {
          return _this.handlerInputValid(event);
        }, false);
      });
    }
  }, {
    key: "firstRun",
    value: function firstRun() {
      this.enablePhoneMask();
    }
  }, {
    key: "enablePhoneMask",
    value: function enablePhoneMask() {
      this.phone = new _iForm_phoneInputMask__WEBPACK_IMPORTED_MODULE_0__["default"]({
        selector: '[data-phone="true"]',
        inputid: "".concat(this.selectorID, "__phone")
      });
    }
  }, {
    key: "handlerClickButton",
    value: function handlerClickButton(event) {
      var _this2 = this;

      this.submitValid = false;
      this.warning = document.querySelectorAll("".concat(this.selectorID, " input.warning"));
      Array.from(this.inputCollect, function (f) {
        return _this2.__inputInvalid(f);
      });
      if (this.warning.length === 0) this.submitValid = true;
      if (this.submitValid) this.__nextSubmit();
    }
  }, {
    key: "handlerInputValid",
    value: function handlerInputValid(event) {
      this.t = event.target;
      if (this.t.value.length >= 2) this.t.classList.remove('warning');
    }
  }, {
    key: "__inputInvalid",
    value: function __inputInvalid(item) {
      this.el = item;
      if (this.el.value.length <= 2) this.el.classList.add('warning');
    }
  }, {
    key: "__nextSubmit",
    value: function __nextSubmit() {
      this.log = console.log('Отправлено!');

      _toConsumableArray(this.inputCollect).forEach(function (el) {
        var currentElem = el;
        currentElem.style.backgroundColor = '#0000';
        currentElem.style.color = '#fff3';
        currentElem.style.pointerEvents = 'none';
        currentElem.setAttribute('readonly', true);
      });

      this.button.style.pointerEvents = 'none';
      this.button.style.backgroundColor = '#5ccc2c';
      this.button.style.color = '#fff';
      this.button.innerText = 'Отправлено!';
      this.button.setAttribute('readonly', true);
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('[MODULE]:', this.name, true);
    }
  }]);

  return $validatorInput;
}();



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2Nvbm5lY3QuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2FjdGl2ZVRhYnMuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2hlYWRlck9ic2VydmVyLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9pRm9ybS9waG9uZUlucHV0TWFzay5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvbW92ZVNjcm9sbFVQLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9vcGVuTXlCdXJnZXIuanMiLCJ3ZWJwYWNrOi8vLy4vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL3NsaWRlckNvbnRyb2xlLmpzIiwid2VicGFjazovLy8uL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy9zbGlkZXJUaGVtZXMvc2xpZGVyU2tpbi5qcyIsIndlYnBhY2s6Ly8vLi9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvdmFsaWRhdG9ySW5wdXQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuXG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcImNvbm5lY3RcIjogMFxuIFx0fTtcblxuIFx0dmFyIGRlZmVycmVkTW9kdWxlcyA9IFtdO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbXCIuL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvY29ubmVjdC5qc1wiLFwicGx1Z2luc1wiXSk7XG4gXHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIHJlYWR5XG4gXHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiIsIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gICAgTmFtZTogaW5kZXguanNcclxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuaW1wb3J0ICRtb3ZlU2Nyb2xsVVAgZnJvbSAnLi9tb2R1bGVzL21vdmVTY3JvbGxVUCc7XHJcbmltcG9ydCAkb3Blbk15QnVyZ2VyIGZyb20gJy4vbW9kdWxlcy9vcGVuTXlCdXJnZXInO1xyXG5pbXBvcnQgJGhlYWRlck9ic2VydmVyIGZyb20gJy4vbW9kdWxlcy9oZWFkZXJPYnNlcnZlcic7XHJcbmltcG9ydCAkc2xpZGVyQ29udHJvbGUgZnJvbSAnLi9tb2R1bGVzL3NsaWRlckNvbnRyb2xlJztcclxuaW1wb3J0ICRhY3RpdmVUYWJzIGZyb20gJy4vbW9kdWxlcy9hY3RpdmVUYWJzJztcclxuaW1wb3J0ICR2YWxpZGF0b3JJbnB1dCBmcm9tICcuL21vZHVsZXMvdmFsaWRhdG9ySW5wdXQnO1xyXG5cclxuY29uc3Qgc3RhcnRQYWNrYWdlID0gKG9wdGlvbikgPT4ge1xyXG5cdGNvbnNvbGUubG9nKCdbRE9NXTonLCAnRE9NQ29udGVudExvYWRlZCcsIG9wdGlvbik7XHJcblxyXG5cdG5ldyAkbW92ZVNjcm9sbFVQKHtcclxuXHRcdHNlbGVjdG9yOiAnLmpzX19tb3ZlU2Nyb2xsVVAnLFxyXG5cdFx0c3BlZWQ6IDhcclxuXHR9KS5ydW4oKTtcclxuXHJcblx0bmV3ICRvcGVuTXlCdXJnZXIoe1xyXG5cdFx0YnVyZ2VyOiAnLmpzX19uYXZIYW1idXJnZXInLFxyXG5cdFx0bmF2YmFyOiAnLmpzX19uYXZIYW1idXJnZXJPcGVuZXInLFxyXG5cdFx0cHJlZml4OiAnbXloZWFkZXInXHJcblx0fSkucnVuKCk7XHJcblxyXG5cclxuXHJcblx0bmV3ICRzbGlkZXJDb250cm9sZSh7XHJcblx0XHRzZWxlY3RvcjogJy5qc19fc29sdXRpb25Db250cm9sJyxcclxuXHRcdHNlbGVjdG9yU2xpZGVyOiAnLmpzX19zbGlkZXJFbmFibGUnLFxyXG5cdFx0cGFzdGVMaXN0OiAnLmpzX19wYXN0ZUxpc3QnLFxyXG5cdFx0Z2V0TGlzdDogJy5qc19fZ2V0TGlzdCcsXHJcblx0XHRhY3RpdmVFbGVtOiAnLmpzX19hY3RpdmVFbGVtJyxcclxuXHRcdHRvdGFsRWxlbTogJy5qc19fdG90YWxFbGVtJyxcclxuXHRcdHByZWZpeDogJ3NvbHV0aW9uJyxcclxuXHRcdHByZVZpZXc6IDZcclxuXHR9KS5ydW4oKTtcclxuXHJcblx0bmV3ICRhY3RpdmVUYWJzKHtcclxuXHRcdHNlbGVjdG9yOiAnLmpzX19wcm9qZWN0VGFicycsXHJcblx0XHRpbmZvQ29udGVudDogJy5qc19fcHJvamVjdFRhYnNJbmZvJyxcclxuXHRcdGRhdGFQYXJhbTogJ2RhdGEtc3RhdGUnXHJcblx0fSkucnVuKCk7XHJcblxyXG5cdG5ldyAkc2xpZGVyQ29udHJvbGUoe1xyXG5cdFx0c2VsZWN0b3I6ICcuanNfX3ByZXNzQ29udHJvbCcsXHJcblx0XHRwcmVmaXg6ICdwcmVzcycsXHJcblx0XHRzbGlkZXJTa2luOiAncHJlc3MnLFxyXG5cdFx0cHJlVmlldzogNFxyXG5cdH0pLnJ1bigpO1xyXG5cdFxyXG5cdG5ldyAkc2xpZGVyQ29udHJvbGUoe1xyXG5cdFx0c2VsZWN0b3I6ICcuanNfX25ld3NDb250cm9sJyxcclxuXHRcdHByZWZpeDogJ25ld3MnLFxyXG5cdFx0c2xpZGVyU2tpbjogJ25ld3MnLFxyXG5cdFx0cHJlVmlldzogMyxcclxuXHRcdHVuaXF1ZTogdHJ1ZVxyXG5cdH0pLnJ1bigpO1xyXG5cclxuXHRuZXcgJHZhbGlkYXRvcklucHV0KHtcclxuXHRcdHNlbGVjdG9yOiAnI2Fuc3dlcidcclxuXHR9KS5ydW4oKTtcclxuXHRcclxuXHRuZXcgJGhlYWRlck9ic2VydmVyKHtcclxuXHRcdHNlbGVjdG9yOiAnLmpzX19oZWFkZXJPYnMnLFxyXG5cdFx0b3V0cHV0OiAnLmpzX19oZWFkZXJPYnNSZXNpemUnXHJcblx0fSkucnVuKCk7XHJcblx0XHJcbn07XHJcblxyXG5jb25zdCBzdGFydCA9IChvcHRpb24pID0+IHtcclxuXHRpZiAob3B0aW9uID09PSB0cnVlKSBzdGFydFBhY2thZ2Uob3B0aW9uKTtcclxuXHRlbHNlIGNvbnNvbGUuZXJyb3IoJ1N5c3RlbTogJywgJ0kgdGhpbmsgc2hpdCBoYXBwZW5zIPCfmKUgJyk7XHJcbn07XHJcblxyXG5jb25zdCBhZGRDc3MgPSAoZmlsZU5hbWUpID0+IHtcclxuXHRjb25zdCBsaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnbGluaycpO1xyXG5cdGxpbmsudHlwZSA9ICd0ZXh0L2Nzcyc7XHJcblx0bGluay5yZWwgPSAnc3R5bGVzaGVldCc7XHJcblx0bGluay5ocmVmID0gZmlsZU5hbWU7XHJcblxyXG5cdGRvY3VtZW50LmhlYWQuYXBwZW5kQ2hpbGQobGluayk7XHJcbn07XHJcblxyXG5pZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93ICYmIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKSB7XHJcblxyXG5cdGlmIChuYXZpZ2F0b3IudXNlckFnZW50LmluY2x1ZGVzKCdGaXJlZm94JykpIGFkZENzcygnY3NzL2ZpcmVmb3guY3NzJyk7XHJcblx0aWYgKG5hdmlnYXRvci51c2VyQWdlbnQuaW5jbHVkZXMoJ0VkZ2UnKSkgYWRkQ3NzKCdjc3MvZWRnZS5jc3MnKTtcclxuXHJcblx0ZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsIHN0YXJ0KHRydWUpLCBmYWxzZSk7XHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgJGFjdGl2ZVRhYnMge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQuc2VsZWN0b3IpO1xyXG5cdFx0aWYgKCF0aGlzLnNlbGVjdG9yKSByZXR1cm47XHJcblx0XHQvLyAtLS0gTkVYVCAtLS1cclxuXHRcdHRoaXMuZXJyb3JUZXh0ID0gJ/CfkoAgT3VjaCwgZGF0YWJhc2UgZXJyb3IuLi4nO1xyXG5cdFx0dGhpcy5pbmZvQ29udGVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5pbmZvQ29udGVudCk7XHJcblx0XHR0aGlzLmRhdGFQYXJhbSA9IGluaXQuZGF0YVBhcmFtO1xyXG5cdFx0XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHsgY29uc29sZS5sb2coJ1tNT0RVTEVdOicsIHRoaXMubmFtZSwgdHJ1ZSk7IH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0aWYgKHRoaXMuc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdHRoaXMuZmlyc3RSdW4oKTtcclxuXHRcdFx0dGhpcy5ldmVudEhhbmRsZXJzKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRldmVudEhhbmRsZXJzKCkge1xyXG5cdFx0dGhpcy5zZWxlY3Rvci5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IHRoaXMubGlzdGVuZXIoZXZlbnQpLCBmYWxzZSk7XHJcblx0fVxyXG5cclxuXHRmaXJzdFJ1bigpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IuZmlyc3RFbGVtZW50Q2hpbGQuc2V0QXR0cmlidXRlKHRoaXMuZGF0YVBhcmFtLCAnaS1hY3RpdmUnKTtcclxuXHRcdHRoaXMuaW5mb0NvbnRlbnQuZmlyc3RFbGVtZW50Q2hpbGQuc2V0QXR0cmlidXRlKHRoaXMuZGF0YVBhcmFtLCAnaS1hY3RpdmUnKTtcclxuXHR9XHJcblxyXG5cdGxpc3RlbmVyKGV2ZW50KSB7XHJcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0dGhpcy5jdXJFbGVtID0gZXZlbnQudGFyZ2V0O1xyXG5cdFx0XHJcblx0XHRpZiAodGhpcy5jdXJFbGVtID09PSB0aGlzLnNlbGVjdG9yKSByZXR1cm47XHJcblx0XHRpZiAodGhpcy5jdXJFbGVtLmhhc0F0dHJpYnV0ZSh0aGlzLmRhdGFQYXJhbSkpIHJldHVybjtcclxuXHRcdFxyXG5cdFx0aWYgKHRoaXMuc2VsZWN0b3IucXVlcnlTZWxlY3RvcihgWyR7dGhpcy5kYXRhUGFyYW19XWApKSB7XHJcblx0XHRcdHRoaXMuc2VsZWN0b3IucXVlcnlTZWxlY3RvcihgWyR7dGhpcy5kYXRhUGFyYW19XWApLnJlbW92ZUF0dHJpYnV0ZSh0aGlzLmRhdGFQYXJhbSk7XHJcblx0XHR9XHJcblx0XHRpZiAodGhpcy5pbmZvQ29udGVudC5xdWVyeVNlbGVjdG9yKGBbJHt0aGlzLmRhdGFQYXJhbX1dYCkpIHtcclxuXHRcdFx0dGhpcy5pbmZvQ29udGVudC5xdWVyeVNlbGVjdG9yKGBbJHt0aGlzLmRhdGFQYXJhbX1dYCkucmVtb3ZlQXR0cmlidXRlKHRoaXMuZGF0YVBhcmFtKTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0dGhpcy5pbmRleCA9IFsuLi50aGlzLnNlbGVjdG9yLmNoaWxkcmVuXS5pbmRleE9mKHRoaXMuY3VyRWxlbSk7XHJcblx0XHRcclxuXHRcdHRoaXMuc2VsZWN0b3IuY2hpbGRyZW5bdGhpcy5pbmRleF0uc2V0QXR0cmlidXRlKHRoaXMuZGF0YVBhcmFtLCAnaS1hY3RpdmUnKTtcclxuXHRcdHRoaXMuaW5mb0NvbnRlbnQuY2hpbGRyZW5bdGhpcy5pbmRleF0uc2V0QXR0cmlidXRlKHRoaXMuZGF0YVBhcmFtLCAnaS1hY3RpdmUnKTtcclxuXHR9XHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgJGhlYWRlck9ic2VydmVyIHtcclxuXHRjb25zdHJ1Y3Rvcihpbml0KSB7XHJcblx0XHR0aGlzLnNlbGVjdG9yID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcihpbml0LnNlbGVjdG9yKTtcclxuXHRcdGlmICghdGhpcy5zZWxlY3RvcikgcmV0dXJuO1xyXG5cdFx0dGhpcy5vdXRwdXRIZWlnaHQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQub3V0cHV0KTtcclxuXHRcdHRoaXMuZ2xvYmFsID0gd2luZG93O1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7IGNvbnNvbGUubG9nKCdbTU9EVUxFXTonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGlmICh0aGlzLnNlbGVjdG9yKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cdFx0XHR0aGlzLmZpcnN0UnVuKCk7XHJcblx0XHRcdHRoaXMuZXZlbnRIYW5kbGVycygpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHRmaXJzdFJ1bigpIHtcclxuXHRcdHNldFRpbWVvdXQoKCkgPT4gdGhpcy5yZXNpemVIZWlnaHQoKSwgMTAwKTtcclxuXHR9XHJcblx0XHJcblx0ZXZlbnRIYW5kbGVycygpIHtcclxuXHRcdHRoaXMuZ2xvYmFsLmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIGV2ZW50ID0+IHRoaXMucmVzaXplSGVpZ2h0KGV2ZW50KSwgZmFsc2UpO1xyXG5cdH1cclxuXHRcclxuXHRyZXNpemVIZWlnaHQoKSB7XHJcblx0XHR0aGlzLndpbmRvd1NpemUgPSB3aW5kb3cuaW5uZXJXaWR0aCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50V2lkdGggfHwgZG9jdW1lbnQuYm9keS5jbGllbnRXaWR0aDtcclxuXHRcdGlmICh0aGlzLndpbmRvd1NpemUgPj0gOTkyKSB0aGlzLm91dHB1dEhlaWdodC5zdHlsZS5oZWlnaHQgPSAnJztcclxuXHRcdGVsc2Uge1xyXG5cdFx0XHR0aGlzLmdldEhlaWdodCA9IHRoaXMuc2VsZWN0b3Iub2Zmc2V0SGVpZ2h0O1xyXG5cdFx0XHR0aGlzLm91dHB1dEhlaWdodC5zdHlsZS5oZWlnaHQgPSBgJHt0aGlzLmdldEhlaWdodH1weGA7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiIsImV4cG9ydCBkZWZhdWx0IGNsYXNzICRwaG9uZUlucHV0TWFzayB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5pbnB1dCA9IGluaXQuaW5wdXRpZDtcclxuXHRcdHRoaXMubXl0YXJnZXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuaW5wdXR9YCk7XHJcblx0XHR0aGlzLnJlZ2V4cCA9IC9eWyt8XFxkP11bLVxccy4oKS8wLTldKiQvO1xyXG5cclxuXHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cdFx0dGhpcy5ldmVudEhhbmRsZXJzKCk7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHsgY29uc29sZS5sb2coJ+KAoiBbQURET05dOicsIHRoaXMubmFtZSwgdHJ1ZSk7IH1cclxuXHJcblx0ZXZlbnRIYW5kbGVycygpIHtcclxuXHRcdHRoaXMubXl0YXJnZXQuYWRkRXZlbnRMaXN0ZW5lcignaW5wdXQnLCBldmVudCA9PiB0aGlzLm1haW4oZXZlbnQpLCBmYWxzZSk7XHJcblx0XHR0aGlzLm15dGFyZ2V0LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCBldmVudCA9PiB0aGlzLndoYXRDb2RlKGV2ZW50KSwgZmFsc2UpO1xyXG5cdH1cclxuXHJcblx0d2hhdENvZGUoZXZlbnQpIHtcclxuXHRcdGlmIChldmVudC5jb2RlID09PSAnQmFja3NwYWNlJykgdGhpcy5teXRhcmdldC52YWx1ZSA9ICcnO1xyXG5cdH1cclxuXHJcblx0bWFpbihldmVudCkge1xyXG5cdFx0Y29uc3Qgc3BhY2UgPSAnICc7XHJcblx0XHRjb25zdCBnZXRQYXR0ZXIgPSBldmVudC50YXJnZXQuZ2V0QXR0cmlidXRlKCdkYXRhLXBhdHRlcm4nKTtcclxuXHRcdGNvbnN0IHJlc3VsdFBhdHRlcm4gPSBuZXcgUmVnRXhwKGBeJHtnZXRQYXR0ZXJ9JGApO1xyXG5cdFx0Y29uc3QgbWFzayA9IHRoaXMubXl0YXJnZXQudmFsdWUucmVwbGFjZSgvXFxEL2csICcnKS5tYXRjaChyZXN1bHRQYXR0ZXJuKTtcclxuXHJcblx0XHRpZiAodGhpcy5teXRhcmdldC52YWx1ZS5sZW5ndGggPT09IDApIHJldHVybjtcclxuXHRcdGlmICghdGhpcy5teXRhcmdldC52YWx1ZS5tYXRjaCh0aGlzLnJlZ2V4cCkpIHtcclxuXHRcdFx0dGhpcy5teXRhcmdldC52YWx1ZSA9IHRoaXMubXl0YXJnZXQudmFsdWUuc2xpY2UoMCwgLTEpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0LyoqXHJcblx0XHRcdCAqIFt2YWx1ZTogaWYgeW91IHNlZSBpdCwgZG9uJ3QgYXNrIG1lOiBcIldoYXQgdGhlIGhlbGwgaXMgdGhpcywgYnJvID8hXVxyXG5cdFx0XHQgKiBAdHlwZSB7W3R5cGVdfVxyXG5cdFx0XHQgKi9cclxuXHRcdFx0dGhpcy5teXRhcmdldC52YWx1ZSA9ICFtYXNrWzJdID8gYCske21hc2tbMV19JHtzcGFjZX1gIDogYCske21hc2tbMV19JHtzcGFjZX1gXHJcblx0XHRcdFx0KyAoIW1hc2tbM10gPyBgKCR7bWFza1syXX1gIDogYCgke21hc2tbMl19KSR7c3BhY2V9YCkgKyBtYXNrWzNdXHJcblx0XHRcdFx0KyAobWFza1s0XSA/IGAtJHttYXNrWzRdfWAgOiAnJylcclxuXHRcdFx0XHQrIChtYXNrWzVdID8gYC0ke21hc2tbNV19YCA6ICcnKTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgJG1vdmVTY3JvbGxVUCB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5zZWxlY3Rvcik7XHJcblx0XHRpZiAoIXRoaXMuc2VsZWN0b3IpIHJldHVybjtcclxuXHJcblx0XHR0aGlzLmNvdW50ID0gTnVtYmVyKE1hdGguYWJzKGluaXQuc3BlZWQpKSB8fCA4O1xyXG5cdFx0dGhpcy5zcGVlZCA9ICh0aGlzLmNvdW50IDw9IDIwKSA/IHRoaXMuY291bnQgOiA4O1xyXG5cdFx0dGhpcy5teVBvcyA9IHdpbmRvdy5wYWdlWU9mZnNldDtcclxuXHRcdHRoaXMuZ2V0U2Nyb2xsID0gMDtcclxuXHRcdHRoaXMuc3BlZWRTY3JvbGwgPSB0aGlzLnNwZWVkO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7IGNvbnNvbGUubG9nKCdbTU9EVUxFXTonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGlmICh0aGlzLnNlbGVjdG9yKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cdFx0XHR0aGlzLmV2ZW50SGFuZGxlcnMoKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGV2ZW50SGFuZGxlcnMoKSB7XHJcblx0XHR0aGlzLnNlbGVjdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZXZlbnQgPT4gdGhpcy5jbGlja2VkQXJyb3coZXZlbnQpLCBmYWxzZSk7XHJcblx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2Nyb2xsJywgZXZlbnQgPT4gdGhpcy5zY3JvbGxlZERvd24oZXZlbnQpLCBmYWxzZSk7XHJcblx0fVxyXG5cclxuXHRjbGlja2VkQXJyb3coZXZlbnQpIHtcclxuXHRcdHRoaXMuZ2V0U2Nyb2xsID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDtcclxuXHRcdGlmICh0aGlzLmdldFNjcm9sbCA+PSAxKSB7XHJcblx0XHRcdHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUodGhpcy5jbGlja2VkQXJyb3cuYmluZCh0aGlzKSk7XHJcblx0XHRcdHdpbmRvdy5zY3JvbGxUbygwLCB0aGlzLmdldFNjcm9sbCAtIHRoaXMuZ2V0U2Nyb2xsIC8gdGhpcy5zcGVlZFNjcm9sbCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRzY3JvbGxlZERvd24oZXZlbnQpIHtcclxuXHRcdHRoaXMubXlQb3MgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcblx0XHRpZiAodGhpcy5teVBvcyA+PSAxMDApIHRoaXMuc2VsZWN0b3IuY2xhc3NMaXN0LmFkZCgnc2hvdycpO1xyXG5cdFx0ZWxzZSB0aGlzLnNlbGVjdG9yLmNsYXNzTGlzdC5yZW1vdmUoJ3Nob3cnKTtcclxuXHR9XHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgJG9wZW5NeUJ1cmdlciB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5idXJnZXIpO1xyXG5cdFx0aWYgKCF0aGlzLnNlbGVjdG9yKSByZXR1cm47XHJcblxyXG5cdFx0dGhpcy5wcmVmaXggPSBpbml0LnByZWZpeDtcclxuXHRcdHRoaXMubmF2YmFyID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcihpbml0Lm5hdmJhcik7XHJcblx0XHR0aGlzLmVycm9yVGV4dCA9ICfwn5KAIE91Y2gsIGRhdGFiYXNlIGVycm9yLi4uJztcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkgeyBjb25zb2xlLmxvZygnW01PRFVMRV06JywgdGhpcy5uYW1lLCB0cnVlKTsgfVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRpZiAodGhpcy5zZWxlY3Rvcikge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHRcdFx0dGhpcy5maXJzdFJ1bigpO1xyXG5cdFx0XHR0aGlzLmV2ZW50SGFuZGxlcnMoKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGV2ZW50SGFuZGxlcnMoKSB7XHJcblx0XHR0aGlzLnNlbGVjdG9yLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZXZlbnQgPT4gdGhpcy5oYW5kbGVhck9wZW5CdXJnZXIoZXZlbnQpLCBmYWxzZSk7XHJcblx0XHR3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsIGV2ZW50ID0+IHRoaXMuaGFuZGxlYXJPcGVuQnVyZ2VyRVNDKGV2ZW50KSwgZmFsc2UpO1xyXG5cdH1cclxuXHRcclxuXHRmaXJzdFJ1bigpIHtcclxuXHRcdHRoaXMuY3JlYXRlTW9iaWxlTGlua3MoKTtcclxuXHR9XHJcblx0XHJcblx0aGFuZGxlYXJPcGVuQnVyZ2VyRVNDKGV2ZW50KSB7XHJcblx0XHRpZiAodGhpcy5uYXZiYXIuY2xhc3NMaXN0LmNvbnRhaW5zKCdvcGVuJykpIHtcclxuXHRcdFx0aWYgKGV2ZW50LmtleSA9PT0gJ0VzY2FwZScpIHRoaXMubmF2YmFyLmNsYXNzTGlzdC5yZW1vdmUoJ29wZW4nKTtcclxuXHRcdH1cclxuXHR9XHJcblx0XHJcblx0aGFuZGxlYXJPcGVuQnVyZ2VyKGV2ZW50KSB7XHJcblx0XHRpZiAodGhpcy5uYXZiYXIuY2xhc3NMaXN0LmNvbnRhaW5zKCdvcGVuJykpIHRoaXMubmF2YmFyLmNsYXNzTGlzdC5yZW1vdmUoJ29wZW4nKTtcclxuXHRcdGVsc2UgdGhpcy5uYXZiYXIuY2xhc3NMaXN0LmFkZCgnb3BlbicpO1xyXG5cdH1cclxuXHJcblx0Y3JlYXRlTW9iaWxlTGlua3MoKSB7XHJcblx0XHRjb25zdCBsaW5rID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzX19oZWFkZXJUZXh0Jyk7XHJcblx0XHRjb25zdCBsaW5rRmlyc3QgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuanNfX2hlYWRlclRleHQnKVswXS5jaGlsZEVsZW1lbnRDb3VudCB8fCAwO1xyXG5cdFx0XHJcblx0XHRjb25zdCByZXN1bHRNYXAgPSAoaW5kZXgpID0+IHtcclxuXHRcdFx0dGhpcy5hciA9IFsuLi5saW5rW2luZGV4XS5jaGlsZHJlbl0ubWFwKGcgPT4gKHtcclxuXHRcdFx0XHRuYW1lOiBnLnRleHRDb250ZW50IHx8ICcnLFxyXG5cdFx0XHRcdHVybDogZy5hdHRyaWJ1dGVzLmhyZWYudGV4dENvbnRlbnQgfHwgJydcclxuXHRcdFx0fSkpO1xyXG5cdFx0XHRyZXR1cm4gdGhpcy5jb3ZuZXJ0QXJyLnB1c2godGhpcy5hcik7XHJcblx0XHR9O1xyXG5cdFx0XHJcblx0XHR0aGlzLmNvdm5lcnRBcnIgPSBbXTtcclxuXHRcdHRoaXMubmV3QXJyID0gWy4uLmxpbmtdLm1hcCgoZiwgaW5kZXgpID0+IHJlc3VsdE1hcChpbmRleCkpO1xyXG5cdFx0XHJcblx0XHR0aGlzLnJlc3VsdCA9IFtdO1xyXG5cdFx0dGhpcy5jb3ZuZXJ0QXJyLnJldmVyc2UoKS5tYXAoZiA9PiB0aGlzLnJlc3VsdC5wdXNoKC4uLmYpKTtcclxuXHRcdFxyXG5cdFx0Y29uc3QgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnRElWJyk7XHJcblx0XHRkaXYuY2xhc3NMaXN0ID0gYCR7dGhpcy5wcmVmaXh9X19ncm91cCAke3RoaXMucHJlZml4fV9fY2xvbmUtbGlzdGA7XHJcblx0XHR0aGlzLmNyZWF0SHJlZiA9IHRoaXMubmF2YmFyLnF1ZXJ5U2VsZWN0b3IoJ0RJVicpLmFwcGVuZENoaWxkKGRpdik7XHJcblx0XHRcclxuXHRcdHRoaXMucmVzdWx0Lm1hcCgoZiwgaW5kZXgpID0+IHtcclxuXHRcdFx0Y29uc3QgbXlIcmVmID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xyXG5cdFx0XHRpZiAoaW5kZXggPCBsaW5rRmlyc3QpIG15SHJlZi5jbGFzc0xpc3QgPSBgJHt0aGlzLnByZWZpeH1fX2Nsb25lLWl0ZW0gJHt0aGlzLnByZWZpeH1fX2Nsb25lLS1zdHlsZWA7XHJcblx0XHRcdGVsc2UgbXlIcmVmLmNsYXNzTGlzdCA9IGAke3RoaXMucHJlZml4fV9fY2xvbmUtaXRlbWA7XHJcblx0XHRcdG15SHJlZi50ZXh0Q29udGVudCA9IGYubmFtZSB8fCAnJztcclxuXHRcdFx0bXlIcmVmLmhyZWYgPSBmLnVybCB8fCAnJztcclxuXHRcdFx0XHJcblx0XHRcdHJldHVybiB0aGlzLmNyZWF0SHJlZi5hcHBlbmRDaGlsZChteUhyZWYpO1xyXG5cdFx0fSk7XHJcblx0fVxyXG59XHJcbiIsImltcG9ydCBHbGlkZSBmcm9tICdAZ2xpZGVqcy9nbGlkZSc7XHJcbmltcG9ydCAkc2xpZGVyU2tpbiBmcm9tICcuL3NsaWRlclRoZW1lcy9zbGlkZXJTa2luJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzICRzbGlkZXJDb250cm9sZSB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5zZWxlY3RvciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5pdC5zZWxlY3Rvcik7XHJcblx0XHRpZiAoIXRoaXMuc2VsZWN0b3IpIHJldHVybjtcclxuXHJcblx0XHR0aGlzLnNlbGVjdG9yU2xpZGVyID0gaW5pdC5zZWxlY3RvclNsaWRlciB8fCAnLmpzX19zbGlkZXJFbmFibGUnO1xyXG5cdFx0dGhpcy5wcmVmaXggPSBpbml0LnByZWZpeDtcclxuXHRcdHRoaXMuZ2V0TGlzdCA9IGluaXQuZ2V0TGlzdCB8fCAnLmpzX19nZXRMaXN0JztcclxuXHRcdHRoaXMucGFzdGVMaXN0ID0gdGhpcy5zZWxlY3Rvci5xdWVyeVNlbGVjdG9yKGluaXQucGFzdGVMaXN0KSB8fCB0aGlzLnNlbGVjdG9yLnF1ZXJ5U2VsZWN0b3IoJy5qc19fcGFzdGVMaXN0Jyk7XHJcblxyXG5cdFx0dGhpcy5hY3RpdmVFbGVtID0gdGhpcy5zZWxlY3Rvci5xdWVyeVNlbGVjdG9yKGluaXQuYWN0aXZlRWxlbSkgfHwgdGhpcy5zZWxlY3Rvci5xdWVyeVNlbGVjdG9yKCcuanNfX2FjdGl2ZUVsZW0nKTtcclxuXHRcdHRoaXMudG90YWxFbGVtID0gdGhpcy5zZWxlY3Rvci5xdWVyeVNlbGVjdG9yKGluaXQudG90YWxFbGVtKSB8fCB0aGlzLnNlbGVjdG9yLnF1ZXJ5U2VsZWN0b3IoJy5qc19fdG90YWxFbGVtJyk7XHJcblx0XHR0aGlzLmdldEJhY2tncm91bmQgPSBpbml0LmJhY2tncm91bmQgfHwgJy5qc19fZ2V0QmFja2dyb3VuZCc7XHJcblxyXG5cdFx0dGhpcy5nbGlkZU51bUluZGV4ID0gMDtcclxuXHRcdHRoaXMuZ2xpZGVOdW1WaWV3ID0gMDtcclxuXHJcblx0XHR0aGlzLnByZVZpZXcgPSBpbml0LnByZVZpZXcgfHwgMTtcclxuXHRcdHRoaXMuc2xpZGVyU2tpbiA9IGluaXQuc2xpZGVyU2tpbiB8fCAnZGVmYXVsdCc7XHJcblx0XHRcclxuXHRcdHRoaXMuc2tpblVuaXF1ZSA9IGluaXQudW5pcXVlIHx8IGZhbHNlO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7IGNvbnNvbGUubG9nKCdbTU9EVUxFXTonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGlmICh0aGlzLnNlbGVjdG9yKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cdFx0XHR0aGlzLmZpcnN0UnVuKCk7XHJcblx0XHRcdHRoaXMuZXZlbnRIYW5kbGVycygpO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0Zmlyc3RSdW4oKSB7XHJcblx0XHR0aGlzLmNvbGxlY3Rpb25MaXN0KCk7XHJcblx0XHR0aGlzLmNyZWF0ZVNsaWRlclNraW4oKTtcclxuXHRcdHRoaXMubW91bnRTbGlkZXIoKTtcclxuXHR9XHJcblxyXG5cdGV2ZW50SGFuZGxlcnMoKSB7XHJcblx0XHR0aGlzLmdsaWRlLm9uKFsnbW91bnQuYWZ0ZXInLCAncnVuJ10sIGV2ZW50ID0+IHRoaXMuX19yZWZyZXNoU2xpZGVyKGV2ZW50KSwgZmFsc2UpO1xyXG5cdFx0dGhpcy5nbGlkZS5vbihbJ3Jlc2l6ZSddLCBldmVudCA9PiB0aGlzLl9fcmVmcmVzaFNsaWRlcihldmVudCksIGZhbHNlKTtcclxuXHR9XHJcblxyXG5cdF9fcmVmcmVzaFNsaWRlcigpIHtcclxuXHRcdHRoaXMuZ2xpZGVOdW1JbmRleCA9IHRoaXMuZ2xpZGUuaW5kZXg7XHJcblx0XHR0aGlzLmdsaWRlTnVtVmlldyA9IHRoaXMuZ2xpZGUuc2V0dGluZ3MucGVyVmlldztcclxuXHJcblx0XHR0aGlzLl9fY2FsY0NvdW50KCk7XHJcblx0fVxyXG5cclxuXHRjb2xsZWN0aW9uTGlzdCgpIHtcclxuXHRcdHRoaXMuYXJyID0gdGhpcy5zZWxlY3Rvci5xdWVyeVNlbGVjdG9yKHRoaXMuZ2V0TGlzdCkuY2hpbGRyZW47XHJcblx0XHR0aGlzLmFyckNvbGxlY3Rpb24gPSBbLi4udGhpcy5hcnJdLm1hcCgoZiwgaW5kZXgpID0+ICh7XHJcblx0XHRcdHRleHQ6IChmLnRleHRDb250ZW50IHx8ICcnKS50cmltKCksXHJcblx0XHRcdHVybDogKGYuZ2V0QXR0cmlidXRlKCdkYXRhLXVybCcpIHx8ICcnKS50cmltKCksXHJcblx0XHRcdHN2ZzogKGYuZ2V0QXR0cmlidXRlKCdkYXRhLXN2Z25hbWUnKSB8fCAnJykudHJpbSgpLFxyXG5cdFx0XHRwb3N0OiAoZi5nZXRBdHRyaWJ1dGUoJ2RhdGEtcG9zdCcpIHx8ICcnKS50cmltKCkucmVwbGFjZSgvXFwvL2dpLCAnPHNwYW4+Lzwvc3Bhbj4nKVxyXG5cdFx0fSkpO1xyXG5cdH1cclxuXHJcblx0X19jYWxjQ291bnQoKSB7XHJcblx0XHRjb25zdCB4ID0gdGhpcy5wYXN0ZUxpc3QuY2hpbGRFbGVtZW50Q291bnQgfHwgMDtcclxuXHRcdHRoaXMudG90YWxFbGVtLnRleHRDb250ZW50ID0geDtcclxuXHJcblx0XHRjb25zdCB5ID0gTnVtYmVyKHRoaXMuZ2xpZGVOdW1JbmRleCArIHRoaXMuZ2xpZGVOdW1WaWV3KSB8fCAwO1xyXG5cdFx0dGhpcy5hY3RpdmVFbGVtLnRleHRDb250ZW50ID0geTtcclxuXHR9XHJcblxyXG5cdGNyZWF0ZVNsaWRlclNraW4oKSB7XHJcblx0XHR0aGlzLmFyckNvbGxlY3Rpb24uZm9yRWFjaChmID0+IG5ldyAkc2xpZGVyU2tpbih7XHJcblx0XHRcdF93aGF0U2tpbjogdGhpcy5zbGlkZXJTa2luLFxyXG5cdFx0XHRfcmVzdWx0OiB0aGlzLnBhc3RlTGlzdCxcclxuXHRcdFx0X3ByZWZpeDogdGhpcy5wcmVmaXgsXHJcblx0XHRcdHRleHQ6IGYudGV4dCxcclxuXHRcdFx0c3ZnOiBmLnN2ZyxcclxuXHRcdFx0dXJsOiBmLnVybCxcclxuXHRcdFx0cG9zdDogZi5wb3N0XHJcblx0XHR9KSk7XHJcblx0XHRcclxuXHRcdGlmICh0aGlzLnNraW5VbmlxdWUpIHtcclxuXHRcdFx0Y29uc29sZS53YXJuKCfigKIgW1dBUk5JTkddIFVuaXF1ZSBza2luIGlzOicsIHRydWUpO1xyXG5cdFx0XHRcclxuXHRcdFx0Y29uc3QgYmFja1BpY3R1cmUgPSB0aGlzLnNlbGVjdG9yLnF1ZXJ5U2VsZWN0b3IodGhpcy5nZXRCYWNrZ3JvdW5kKTtcclxuXHRcdFx0aWYgKCFiYWNrUGljdHVyZSkge1xyXG5cdFx0XHRcdGNvbnNvbGUuZXJyb3IoYFVuaXF1ZSBza2luIGlzOiB0cnVlLCBidXQgJHt0aGlzLmdldEJhY2tncm91bmR9IG5vdCBmb3VuZCFgKTtcclxuXHRcdFx0XHRyZXR1cm47XHJcblx0XHRcdH1cclxuXHRcdFx0dGhpcy5nZXRBdHRyID0gYmFja1BpY3R1cmUuZ2V0QXR0cmlidXRlKCdkYXRhLWJhY2tncm91bmQnKTtcclxuXHJcblx0XHRcdGNvbnN0IGZpcnN0RWxlbSA9IHRoaXMucGFzdGVMaXN0LmZpcnN0RWxlbWVudENoaWxkLnF1ZXJ5U2VsZWN0b3IoJ0EnKTtcclxuXHJcblx0XHRcdGNvbnN0IHVuaXF1ZURJViA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ0RJVicpO1xyXG5cdFx0XHR1bmlxdWVESVYuY2xhc3NMaXN0ID0gYCR7dGhpcy5wcmVmaXh9X19lbC0tYmFja2dyb3VuZGA7XHJcblx0XHRcdHVuaXF1ZURJVi5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2UgPSBgdXJsKCR7dGhpcy5nZXRBdHRyIHx8ICcnfSlgO1xyXG5cclxuXHRcdFx0Zmlyc3RFbGVtLnByZXBlbmQodW5pcXVlRElWKTtcclxuXHRcdFx0Zmlyc3RFbGVtLmNsYXNzTGlzdC5hZGQoYCR7dGhpcy5wcmVmaXh9X191bmlxdWVgKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdG1vdW50U2xpZGVyKCkge1xyXG5cdFx0Y29uc3Qga2V5c051bWIgPSBbLi4uQXJyYXkoNSkua2V5cygpXS5tYXAoZiA9PiBNYXRoLnJvdW5kKDI1NjAgLyAoZiArIDEpKSk7XHJcblxyXG5cdFx0dGhpcy5icmVha1BvaW50ID0ge307XHJcblx0XHRrZXlzTnVtYi5tYXAoKGYsIGluZGV4KSA9PiB7XHJcblx0XHRcdHRoaXMuZW5kID0gKHRoaXMucHJlVmlldyAtIGluZGV4KTtcclxuXHRcdFx0aWYgKHRoaXMuZW5kIDw9IDApIHRoaXMuZW5kID0gMTtcclxuXHRcdFx0cmV0dXJuICh0aGlzLmJyZWFrUG9pbnRbZl0gPSB7IHBlclZpZXc6IHRoaXMuZW5kIH0pO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0dGhpcy5nbGlkZSA9IG5ldyBHbGlkZSh0aGlzLnNlbGVjdG9yLnF1ZXJ5U2VsZWN0b3IodGhpcy5zZWxlY3RvclNsaWRlciksIHtcclxuXHRcdFx0dHlwZTogJ3NsaWRlcicsXHJcblx0XHRcdHN0YXJ0QXQ6IDAsXHJcblx0XHRcdHBlclZpZXc6IHRoaXMucHJlVmlldyxcclxuXHRcdFx0Ym91bmQ6IHRydWUsXHJcblx0XHRcdGdhcDogMjEsXHJcblx0XHRcdHJld2luZDogZmFsc2UsXHJcblx0XHRcdGFuaW1hdGlvbkR1cmF0aW9uOiAzMDAsXHJcblx0XHRcdGJyZWFrcG9pbnRzOiB0aGlzLmJyZWFrUG9pbnRcclxuXHRcdH0pLm1vdW50KCk7XHJcblxyXG5cdFx0dGhpcy5nbGlkZU51bUluZGV4ID0gdGhpcy5nbGlkZS5pbmRleDtcclxuXHRcdHRoaXMuZ2xpZGVOdW1WaWV3ID0gdGhpcy5nbGlkZS5zZXR0aW5ncy5wZXJWaWV3O1xyXG5cdFx0dGhpcy5fX2NhbGNDb3VudCgpO1xyXG5cdH1cclxufVxyXG4iLCJleHBvcnQgZGVmYXVsdCBjbGFzcyAkc2xpZGVyU2tpbiB7XHJcblx0Y29uc3RydWN0b3IoaW5pdCkge1xyXG5cdFx0dGhpcy5za2luID0gaW5pdC5fd2hhdFNraW47XHJcblx0XHR0aGlzLnJlc3VsdCA9IGluaXQuX3Jlc3VsdDtcclxuXHRcdHRoaXMucHJlZml4ID0gaW5pdC5fcHJlZml4O1xyXG5cdFx0dGhpcy50ZXh0ID0gaW5pdC50ZXh0O1xyXG5cdFx0dGhpcy5zdmcgPSBpbml0LnN2ZztcclxuXHRcdHRoaXMudXJsID0gaW5pdC51cmw7XHJcblx0XHR0aGlzLnBvc3QgPSBpbml0LnBvc3Q7XHJcblx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHRcdHRoaXMubWFpbigpO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7IGNvbnNvbGUubG9nKCfigKIgW0FERE9OXTonLCB0aGlzLm5hbWUsIHRydWUpOyB9XHJcblxyXG5cdG1haW4oKSB7XHJcblx0XHR0aGlzLnRoZW1lcyA9IHtcclxuXHRcdFx0ZGVmYXVsdDogYFxyXG5cdFx0XHRcdDxhIGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2VsXCIgdGl0bGU9XCJVUkxcIiByb2xlPVwibGlua1wiIGhyZWY9XCIke3RoaXMudXJsfVwiPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19lbC1pdGVtXCI+XHJcblx0XHRcdFx0XHRcdDxvYmplY3QgY2xhc3M9XCIke3RoaXMucHJlZml4fV9fZWwtc3ZnXCI+XHJcblx0XHRcdFx0XHRcdDxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfXyR7dGhpcy5zdmd9XCI+XHJcblx0XHRcdFx0XHRcdDx1c2UgeGxpbms6aHJlZj1cIiNpZC1nbHlwaHMtJHt0aGlzLnN2Z31cIj48L3VzZT5cclxuXHRcdFx0XHRcdFx0PC9zdmc+PC9vYmplY3Q+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCIke3RoaXMucHJlZml4fV9fZWwtaXRlbVwiPlxyXG5cdFx0XHRcdFx0XHQ8cCBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19kZXNjXCI+JHt0aGlzLnRleHR9PC9wPlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PC9hPlxyXG5cdFx0XHRgLFxyXG5cdFx0XHRwcmVzczogYFxyXG5cdFx0XHRcdDxhIGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2VsXCIgdGl0bGU9XCJVUkxcIiByb2xlPVwibGlua1wiIGhyZWY9XCIke3RoaXMudXJsfVwiPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19lbC1pdGVtXCI+XHJcblx0XHRcdFx0XHRcdDxwIGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2RhdGFcIj4ke3RoaXMucG9zdH08L3A+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCIke3RoaXMucHJlZml4fV9fZWwtaXRlbVwiPlxyXG5cdFx0XHRcdFx0XHQ8cCBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19kZXNjXCI+JHt0aGlzLnRleHR9PC9wPlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2VsLWl0ZW1cIj5cclxuXHRcdFx0XHRcdFx0PG9iamVjdCBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19lbC1zdmdcIj5cclxuXHRcdFx0XHRcdFx0PHN2ZyByb2xlPVwicGljdHVyZS1zdmdcIiBjbGFzcz1cImdseXBoc19fJHt0aGlzLnN2Z31cIj5cclxuXHRcdFx0XHRcdFx0PHVzZSB4bGluazpocmVmPVwiI2lkLWdseXBocy0ke3RoaXMuc3ZnfVwiPjwvdXNlPlxyXG5cdFx0XHRcdFx0XHQ8L3N2Zz48L29iamVjdD5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdDwvYT5cclxuXHRcdFx0YCxcclxuXHRcdFx0bmV3czogYFxyXG5cdFx0XHRcdDxhIGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2VsXCIgdGl0bGU9XCJVUkxcIiByb2xlPVwibGlua1wiIGhyZWY9XCIjXCI+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2VsLWl0ZW1cIj5cclxuXHRcdFx0XHRcdFx0PHAgY2xhc3M9XCIke3RoaXMucHJlZml4fV9fZGF0YVwiPiR7dGhpcy5wb3N0fTwvcD5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzcz1cIiR7dGhpcy5wcmVmaXh9X19lbC1pdGVtXCI+XHJcblx0XHRcdFx0XHRcdDxwIGNsYXNzPVwiJHt0aGlzLnByZWZpeH1fX2Rlc2NcIj4ke3RoaXMudGV4dH08L3A+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3M9XCIke3RoaXMucHJlZml4fV9fZWwtaXRlbVwiPlxyXG5cdFx0XHRcdFx0XHQ8b2JqZWN0IGNsYXNzPVwiY2FyZF9fc3ZnLWFycm93XCI+XHJcblx0XHRcdFx0XHRcdDxzdmcgcm9sZT1cInBpY3R1cmUtc3ZnXCIgY2xhc3M9XCJnbHlwaHNfX2Fycm93LXJpZ2h0XCI+XHJcblx0XHRcdFx0XHRcdDx1c2UgeGxpbms6aHJlZj1cIiNpZC1nbHlwaHMtYXJyb3ctcmlnaHRcIj48L3VzZT48L3N2Zz5cclxuXHRcdFx0XHRcdFx0PC9vYmplY3Q+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8L2E+XHJcblx0XHRcdGBcclxuXHRcdH07XHJcblxyXG5cdFx0aWYgKHRoaXMuc2tpbikgdGhpcy5hY3RpdmVUaGVtZSA9IHRoaXMudGhlbWVzW3RoaXMuc2tpbl07XHJcblxyXG5cdFx0Y29uc3QgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnRElWJyk7XHJcblx0XHRkaXYuY2xhc3NMaXN0ID0gJ2dsaWRlX19zbGlkZSc7XHJcblx0XHRkaXYuaW5uZXJIVE1MID0gdGhpcy5hY3RpdmVUaGVtZSB8fCB0aGlzLnRoZW1lcy5kZWZhdWx0O1xyXG5cclxuXHRcdHRoaXMucmVzdWx0LmFwcGVuZENoaWxkKGRpdik7XHJcblx0fVxyXG59XHJcbiIsImltcG9ydCAkcGhvbmVJbnB1dE1hc2sgZnJvbSAnLi9pRm9ybS9waG9uZUlucHV0TWFzayc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyAkdmFsaWRhdG9ySW5wdXQge1xyXG5cdGNvbnN0cnVjdG9yKGluaXQpIHtcclxuXHRcdHRoaXMuc2VsZWN0b3IgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGluaXQuc2VsZWN0b3IpO1xyXG5cdFx0dGhpcy5zZWxlY3RvcklEID0gaW5pdC5zZWxlY3RvcjtcclxuXHRcdHRoaXMuc3VibWl0VmFsaWQgPSBmYWxzZTtcclxuXHJcblx0XHR0aGlzLmJ1dHRvbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5zZWxlY3RvcklEfSBidXR0b25gKTtcclxuXHRcdHRoaXMuaW5wdXRDb2xsZWN0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgJHt0aGlzLnNlbGVjdG9ySUR9IGlucHV0YCk7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHsgY29uc29sZS5sb2coJ1tNT0RVTEVdOicsIHRoaXMubmFtZSwgdHJ1ZSk7IH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0aWYgKHRoaXMuc2VsZWN0b3IpIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdHRoaXMuZmlyc3RSdW4oKTtcclxuXHRcdFx0dGhpcy5ldmVudEhhbmRsZXJzKCk7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRldmVudEhhbmRsZXJzKCkge1xyXG5cdFx0dGhpcy5idXR0b24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiB0aGlzLmhhbmRsZXJDbGlja0J1dHRvbihldmVudCksIGZhbHNlKTtcclxuXHRcdEFycmF5LmZyb20odGhpcy5pbnB1dENvbGxlY3QsIGYgPT4gZi5hZGRFdmVudExpc3RlbmVyKCdpbnB1dCcsIGV2ZW50ID0+IHRoaXMuaGFuZGxlcklucHV0VmFsaWQoZXZlbnQpLCBmYWxzZSkpO1xyXG5cdH1cclxuXHJcblx0Zmlyc3RSdW4oKSB7XHJcblx0XHR0aGlzLmVuYWJsZVBob25lTWFzaygpO1xyXG5cdH1cclxuXHJcblx0ZW5hYmxlUGhvbmVNYXNrKCkge1xyXG5cdFx0dGhpcy5waG9uZSA9IG5ldyAkcGhvbmVJbnB1dE1hc2soe1xyXG5cdFx0XHRzZWxlY3RvcjogJ1tkYXRhLXBob25lPVwidHJ1ZVwiXScsXHJcblx0XHRcdGlucHV0aWQ6IGAke3RoaXMuc2VsZWN0b3JJRH1fX3Bob25lYFxyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRoYW5kbGVyQ2xpY2tCdXR0b24oZXZlbnQpIHtcclxuXHRcdHRoaXMuc3VibWl0VmFsaWQgPSBmYWxzZTtcclxuXHRcdHRoaXMud2FybmluZyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYCR7dGhpcy5zZWxlY3RvcklEfSBpbnB1dC53YXJuaW5nYCk7XHJcblxyXG5cdFx0QXJyYXkuZnJvbSh0aGlzLmlucHV0Q29sbGVjdCwgZiA9PiB0aGlzLl9faW5wdXRJbnZhbGlkKGYpKTtcclxuXHJcblx0XHRpZiAodGhpcy53YXJuaW5nLmxlbmd0aCA9PT0gMCkgdGhpcy5zdWJtaXRWYWxpZCA9IHRydWU7XHJcblx0XHRpZiAodGhpcy5zdWJtaXRWYWxpZCkgdGhpcy5fX25leHRTdWJtaXQoKTtcclxuXHR9XHJcblxyXG5cdGhhbmRsZXJJbnB1dFZhbGlkKGV2ZW50KSB7XHJcblx0XHR0aGlzLnQgPSBldmVudC50YXJnZXQ7XHJcblx0XHRpZiAodGhpcy50LnZhbHVlLmxlbmd0aCA+PSAyKSB0aGlzLnQuY2xhc3NMaXN0LnJlbW92ZSgnd2FybmluZycpO1xyXG5cdH1cclxuXHJcblx0X19pbnB1dEludmFsaWQoaXRlbSkge1xyXG5cdFx0dGhpcy5lbCA9IGl0ZW07XHJcblx0XHRpZiAodGhpcy5lbC52YWx1ZS5sZW5ndGggPD0gMikgdGhpcy5lbC5jbGFzc0xpc3QuYWRkKCd3YXJuaW5nJyk7XHJcblx0fVxyXG5cclxuXHRfX25leHRTdWJtaXQoKSB7XHJcblx0XHR0aGlzLmxvZyA9IGNvbnNvbGUubG9nKCfQntGC0L/RgNCw0LLQu9C10L3QviEnKTtcclxuXHRcdFsuLi50aGlzLmlucHV0Q29sbGVjdF0uZm9yRWFjaCgoZWwpID0+IHtcclxuXHRcdFx0Y29uc3QgY3VycmVudEVsZW0gPSBlbDtcclxuXHRcdFx0Y3VycmVudEVsZW0uc3R5bGUuYmFja2dyb3VuZENvbG9yID0gJyMwMDAwJztcclxuXHRcdFx0Y3VycmVudEVsZW0uc3R5bGUuY29sb3IgPSAnI2ZmZjMnO1xyXG5cdFx0XHRjdXJyZW50RWxlbS5zdHlsZS5wb2ludGVyRXZlbnRzID0gJ25vbmUnO1xyXG5cdFx0XHRjdXJyZW50RWxlbS5zZXRBdHRyaWJ1dGUoJ3JlYWRvbmx5JywgdHJ1ZSk7XHJcblx0XHR9KTtcclxuXHJcblx0XHR0aGlzLmJ1dHRvbi5zdHlsZS5wb2ludGVyRXZlbnRzID0gJ25vbmUnO1xyXG5cdFx0dGhpcy5idXR0b24uc3R5bGUuYmFja2dyb3VuZENvbG9yID0gJyM1Y2NjMmMnO1xyXG5cdFx0dGhpcy5idXR0b24uc3R5bGUuY29sb3IgPSAnI2ZmZic7XHJcblx0XHR0aGlzLmJ1dHRvbi5pbm5lclRleHQgPSAn0J7RgtC/0YDQsNCy0LvQtdC90L4hJztcclxuXHRcdHRoaXMuYnV0dG9uLnNldEF0dHJpYnV0ZSgncmVhZG9ubHknLCB0cnVlKTtcclxuXHR9XHJcbn1cclxuIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ3ZKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVJBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQURBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFGQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOzs7QUFyQ0E7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1hBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUF6QkE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1JBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7OztBQUlBO0FBSUE7QUFDQTs7O0FBOUJBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNWQTs7O0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFFQTs7O0FBMUJBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1pBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFFQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7OztBQTdEQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1ZBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUFBO0FBTUE7OztBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUNBO0FBU0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFSQTtBQVdBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQXRHQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUJBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFhQTtBQWdCQTtBQTlCQTtBQWdEQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7OztBQTFEQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDYkE7QUFDQTtBQUNBOzs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7OztBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBN0RBO0FBQUE7QUFBQTs7Ozs7Ozs7OztBIiwic291cmNlUm9vdCI6IiJ9